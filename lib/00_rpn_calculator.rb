class RPNCalculator    
    def initialize
        @array = []
    end
    
    def push(input)
        @array << input
    end
    
    def value
        @array.last
    end
    
    def plus
        operation(:+)
    end
    
    def minus
        operation(:-)
    end
    
    def times
        operation(:*)
    end
    
    def divide
        operation(:/)
    end
    
    def tokens(string)
        @nums = Array.new
        num_ops = string.split
        
        num_ops.each do |elem|
            if "+-/*".include?(elem)
                @nums << elem.to_sym
            else
                @nums << elem.to_i
            end
        end
        
        @nums
    end
                 
    def evaluate(string)
        op_array = self.tokens(string)
                 
        op_array.each do |elem|
            case elem
            when Integer
                self.push(elem)
            else
                self.operation(elem)
            end
        end
                 
        self.value
    end
    
    def operation(sym)
        raise "calculator is empty" if @array.length == 0
        
        second = @array.pop
        first = @array.pop
        
        case sym
        when :-
            @array << first - second
        when :+
            @array << first + second
        when :*
            @array << first * second
        when :/
            @array << first.fdiv(second)
        end
        
    end
    
end
